/**
 * `js` 识别 `.vue` 文件
 */
declare module '*.vue' {
  import { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

/**
 * 扩展环境变量 
 * - 引用时有提示
 */
interface ImportMetaEnv {
  readonly VITE_SERVE: string
  readonly VITE_BASE_API: string
  readonly VITE_PORT: number
  readonly VITE_DOWNLOAD: string
  readonly VITE_PUBLIC_PATH: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
