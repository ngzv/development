import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
// import VueDevTools from 'vite-plugin-vue-devtools'

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { VantResolver, NaiveUiResolver } from 'unplugin-vue-components/resolvers'
import DefineOptions from 'unplugin-vue-define-options/dist/vite'
import viteCompression from 'vite-plugin-compression'
import { visualizer } from 'rollup-plugin-visualizer'

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  // 在配置中使用环境变量
  // 根据当前工作目录中的 `mode` 加载 .env 文件
  // 设置第三个参数为 '' 来加载所有环境变量，而不管是否有 `VITE_` 前缀
  let env = loadEnv(mode, process.cwd(), '')
  // 当前时间戳 防止缓存
  let time = new Date().getTime()
  return {
    base: env.VITE_PUBLIC_PATH || '/',
    build: {
      // 打包后的文件夹
      outDir: 'dist',
      target: 'es2015',
      gzipSize: true,
      // 关闭生成map文件 可以达到缩小打包体积
      // 这个生产环境一定要关闭，不然打包的产物会很大
      sourcemap: false,
      // chunk 大小警告的限制（单位 KB）
      chunkSizeWarningLimit: 1024,
      // 启用/禁用 gzip 压缩大小报告 
      reportCompressedSize: false,
      // 如果设置为false，整个项目中的所有 CSS 将被提取到一个 CSS 文件中
      cssCodeSplit: true,
      // terser
      minify: 'terser',
      terserOptions: {
        compress: {
          // 生产环境时移除 console
          drop_console: true,
          drop_debugger: true,
          pure_funcs: ['console.log']
        },
        output: {
          // 去掉注释内容
          comments: true
        }
      },
      rollupOptions: {
        // input 入口位置，默认为 `src/main.js`，一般不写
        // 指定输出配置对象
        output: {
          // 静态资源文件
          // 根据文件后缀生成文件夹，对 dist 目录静态资源文件进行归类 
          // ==> `[ext]/[name]-[hash]-${time}.[ext]`
          assetFileNames: `assets/[name]-[hash]-${time}.[ext]`,
          // 引入文件名的名称
          chunkFileNames: `chunks/[name]-[hash]-${time}.js`,
          // 包的入口文件名称
          entryFileNames: `js/[name]-[hash]-${time}.js`,
          // (自动) 拆分代码，这个就是分包
          manualChunks(id) {
            if (id.includes("node_modules")) {
              // 让每个插件都打包成独立的文件
              return id.toString().split("node_modules/")[1].split("/")[0].toString();
            }
          }
        }
      }
    },
    plugins: [
      vue(),
      // VueDevTools(),
      AutoImport({
        // 自动导入系统的 如 ref、reactive、toRef、shallowRef、onMounted 等
        // '@vueuse/core' 自动释放
        imports: [
          'vue',
          'vue-router',
          '@vueuse/core',
          {
            'naive-ui': [
              'useDialog',
              'useMessage',
              'useNotification',
              'useLoadingBar'
            ]
          }
        ],
        // 自动生成声明文件
        // 用来自动导入自定义的文件（但是此方法不利于查找路径源文件）
        // dts: 'src/auto-imports.d.ts',
        dts: false,
        // NaiveUi 按需自动引入
        resolvers: [VantResolver(), NaiveUiResolver()],
      }),
      // NaiveUi 按需自动引入
      Components({
        resolvers: [VantResolver(), NaiveUiResolver()]
      }),
      DefineOptions(),
      viteCompression({
        // 是否在控制台中输出压缩结果
        verbose: true,
        // 是否禁用 - 开启压缩
        disable: false,
        // 压缩前最小文件大小
        threshold: 10240,
        // 压缩算法，可选['gzip'，' brotliccompress '，'deflate '，'deflateRaw']
        algorithm: 'gzip',
        // 文件类型
        ext: '.gz',
        // 源文件压缩后是否删除（根据服务器情况选择，压缩的服务器需配置）
        deleteOriginFile: false
      }),
      // 将 visualizer 插件放到最后
      visualizer({
        // 打包后自动开启分析页面
        open: true
      }),
    ],
    resolve: {
      // 文件系统路径别名
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      host: '0.0.0.0',
      port: env.VITE_PORT,
      open: true,
      proxy: {
        '/dev': {
          // 要请求的后台地址
          target: env.VITE_SERVE,
          // 是否跨域
          changeOrigin: true,
          // 路径重写 把前面的包含的 api 去掉
          rewrite: (path) => path.replace(/^\/api/, ''),
          // 设置响应标头查看转发路径
          bypass(request, response, options) {
            let proxy = options.target + options.rewrite(request.url)
            response.setHeader('x-response-proxy-url', proxy)
          }
        },
        '/gateway': {
          target: env.VITE_SERVER,
          changeOrigin: false
        }
      }
    },
    // 设置最终构建的浏览器兼容目标
    target: ['modules']
  }
})
