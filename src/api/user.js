import { request } from '@/utils'

const module = 'user'

// 用户登录
// export const login = (param) => request.post('/user/login', param, {isNoNeedToken: true})
// export const login = (param) => request.post(`/${module}/login`, param, {isNoNeedToken: true})
export function login(param) {
  return request({
    url: `/${module}/login`,
    method: 'post',
    data: param,
    isNoNeedToken: true
  })
}
