import * as user from '@/api/user'

/**
 * 导出所有
 * 用法:
 * - `import api from '@/api'`
 * - `api.user.xxx`
 * 或
 * - `import api from '@/api/user.js'`
 * - `api.xxx`
 */
export default {
  user
}