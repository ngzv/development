import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import api from '@/api'
import { setToken, clearToken, setLoginMethod, clearLoginMethod } from '@/utils'

// 创建用户相关小仓库
export const useUserStore = defineStore('user', () => {

  const count = ref(0)
  const doubleCount = computed(() => count.value * 2)
  function increment() {
    count.value++
  }

  // 1. 定义管理用户数据的 state
  const token = ref('')

  // 2. 定义获取接口数据的 action 函数
  async function netLogin(param) {
    let res = await api.user.login(param)
    // 成功 200 --> token
    // 失败 201 --> 失败错误信息
    if (res.code === 200) {
      token.value = res.data.token
      // 本地存储持久化存储
      setToken(res.data.token, true)
      setLoginMethod('')
      return res.code
    } else {
      return Promise.reject(new Error(res.message))
    }
  }

  // 清除数据
  function clear() {
    token.value = ''
    clearToken()
    clearLoginMethod()
  }

  // 3. 以对象的格式把 state 和 action return
  return {
    token,
    netLogin,
    clear,
  }
})
