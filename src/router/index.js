import { createRouter, createWebHashHistory } from 'vue-router'
import routes from './routes'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,  // key 与 value 一致简写
  scrollBehavior() {
    return {
      top: 0,
      left: 0
    }
  }
})

export default router
