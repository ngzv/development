import router from '@/router'

import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

import { useUserStore } from '@/stores/user'

// 禁用进度环
NProgress.configure({
  showSpinner: false
})

// 全局前置守卫 访问某个路由之前守卫
// - to: 将要访问哪个路由
// - from: 从那个路由来
// - next: 路由放行函数
router.beforeEach((to, from, next) => {

  NProgress.start()

  // 登陆拦截操作（根据路由 meta 参数是否进入此流程）
  let userStore = useUserStore()
  if (to.matched.some(m => m.meta.auth)) {
    if (userStore.isLogin) {
      // 登录直接放行
      next()
    } else {
      // 未登录先去登录
      next('/login')
    }
  } else {
    next()
  }

  document.title = 'Development ' + to.meta.title
})

// 全局后置守卫
router.afterEach((to, from) => {
  NProgress.done()
})
