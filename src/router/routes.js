const routes = [
  {
    name: 'layout',
    path: '/',
    component: () => import('@/layout/index.vue'),
    // redirect: '/welcome',
    meta: {
      title: '',
      auth: false,
    }
  },
  {
    name: 'login',
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    meta: {
      title: '登录',
      auth: false,
    }
  },
  {
    name: 'any',  // name: '*',
    path: '/:pathMatch(.*)*',  // path: '/:w+',
    component: () => import('@/views/any/index.vue'),
    meta: {
      title: '未知',
      auth: false,
    }
  }
]

export default routes
