/**
 * Created by Ng on 2023/11/09
 */

/**
 * @description 校验首尾是否有空格
 * @param {String} value
 * @returns {Boolean}
 */
export function validateSpace(value) {
  let regular = /(^ | $)/
  return regular.test(value)
}

/**
 * @description 判读是否为外链
 * @param {String} path
 * @returns {Boolean}
 */
export function validateExternal(path) {
  let regular = /^(https?:|mailto:|tel:)/
  return regular.test(path)
}

/**
 * @description 校验手机号
 * @param {String} phone
 * @returns {Boolean}
 */
export function validatePhone(phone) {
  // 也可写为 new RegExp('^1[3456789]\d{9}$')
  let regular = /^1[3456789]\d{9}$/
  return regular.test(phone)
}

/**
 * @description 校验用户名
 * @param {String} uname
 * @returns {Boolean}
 */
export function validateUsername(uname) {
  let regular = /^[a-zA-Z0-9_]{3,12}$/
  return regular.test(uname)
}

/**
 * @description 校验用户密码
 * @param {String} pwd
 * @returns {Boolean}
 */
export function validatePassword(pwd) {
  // [!-~] 的意思包含特殊字符 
  let regular = /^[a-zA-Z0-9!-~]{6,12}$/
  return regular.test(pwd)
}

/**
 * @description 校验用户身份证
 * @param {String} idCard
 * @returns {Boolean}
 */
export function validateIDCard(idCard) {
  let regular = /^\d{15}|(\d{17}(\d|x|X))$/
  return regular.test(idCard)
}

/**
 * @description 校验 Email 邮箱
 * @param {String} email
 * @returns {Boolean}
 */
export function validateEmail(email) {
  // let regular = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/
  let regular = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return regular.test(email)
}

/**
 * @description 校验是否是 URL
 * @param {String} path
 * @returns {Boolean}
 */
export function validateUrl(path) {
  let regular = /(((^https?:(?:\/\/)?)(?:[-;:&=+$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=+$,\w]+@)[A-Za-z0-9.-]+)((?:\/[+~%/.\w-_]*)?\??(?:[-+=&;%@.\w_]*)#?(?:[\w]*))?)$/
  return regular.test(path)
}

/**
 * @description 校验是否包含两位小数
 * @param {String} value
 * @returns {Boolean}
 */
export function validateNumer(value) {
  let regular = /^(([1-9]{1}\d*)|(0{1}))(\.\d{1,2})?$/
  return regular.test(value)
}

/**
 * @description 校验是否包含中文
 * @param {String} value 
 * @returns {Boolean}
 */
export function validateCNChars(value) {
  let regular = /[\u4e00-\u9fa5]/
  return regular.test(value)
}
