/**
 * Created by Ng on 2023/11/13
 */

export const config = {
  // 加载时显示文字
  loadingText: '正在加载中..',
  // 不需要 token 校验的路由
  whiteList: ['/login', '/register', '/any']
}
